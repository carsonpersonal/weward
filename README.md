# WeWard API/DB Project

This is a project that demonstrates a basic knowledge of Docker, Docker Compose, a bit of Python and PostGres, general application infrastructure and simple API calls.

## Instructions to run

#### Prerequisites

 * You should have root access to the machine where you plan to run the application
 * On the same machine, you should have a file containing your intended DB password, located at `/var/local/db_pass.txt`. A best practice would be to lock down the file with root 400 privileges
 * The machine should also have git, docker compose and python all installed

#### Steps to deploy to localhost

1. Clone the repository in a directory of your choice
2. Within the new weward directory, create a new file called `.env` then add the following variables to the file using whatever appropriate values you like
```
PG_USER=<postgres user>
PG_DB=<postgres db name>
```
2. Now run the command `sudo docker-compose build`
3. If there are no errors, run `sudo docker-compose up`

This should give you a live readout of the logs from both the app and db services

#### Test it out

1. Since your database starts empty, the first test should be to add a row of data. Open a new terminal and issue this command:
```    
curl -X POST http://localhost:5000/products -H "Content-Type: application/json" -d "{\"product_name\": \"Super Cool Product\", \"price\": \"49.99\"}"
```
2. You should see a message saying the row was inserted correctly
3. Now run this command to see the contents of your table:
```
curl -X GET http://localhost:5000/products
```
4. You should see a json-formatted listing of your table's contents

#### Additional options

* If you would like to connect a DB management tool to your DB, you will have to go into `docker-compose.yml` and uncomment the port section of the DB; but keep in mind, as a best practice, the DB should not be accessible directly via the network


## Possible improvements

* The password file location could be parameterized
* The data model could be expanded a bit to add complexity to the example
* A wrapper could be created to check if the password file is appropriately locked down
* The implementation could be setup to not run as root, as a security best practice, though you sometimes have to sacrifice flexibility to make such a change

## Technologies used

* Docker and Docker Compose as the main container and container orchestration technologies
* The app is a Python script using Flask and Flask's version of SQL Alchemy for the API and ORM
* The database is PostGres
* GitLab is used as the repository
* ChatGPT 4 was used as a tool to help with some of the coding and to help with unfamiliar technologies (namely Flask and SQLAlchemy)

## Author

Carson Fry - carson.l.fry@gmail.com