import os

# Using Flask and Flask's ingrained form of SQLAlchemy
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

# Get system env variables for PG connection
pg_user = os.environ['POSTGRES_USER']
pg_db = os.environ['POSTGRES_DB']

# Get PG DB from file
with open('/run/secrets/db_pass') as f:
    pg_pass = f.read().strip()

# Connecting PG to Flask/SQLAlchemy	
pg_conn = f'postgresql://{pg_user}:{pg_pass}@db/{pg_db}'
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = pg_conn
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# Defining our very simple example DB
class Product(db.Model):
    __tablename__ = 'products'
    product_id = db.Column(db.Integer, primary_key=True)
    product_name = db.Column(db.String, nullable=False)
    price = db.Column(db.Numeric, nullable=False)

# Define GET method
@app.route('/products', methods=['GET'])
def get_products():
    products = Product.query.all()
    return jsonify(products=[{"product_id": p.product_id, "product_name": p.product_name, "price": str(p.price)} for p in products])

# Define POST method
@app.route('/products', methods=['POST'])
def add_product():
    data = request.get_json()
    new_product = Product(
        product_name=data['product_name'],
        price=data['price']
    )
    db.session.add(new_product)
    db.session.commit()
    return jsonify({"message": "Product added successfully!"}), 201

# Create tables upon app start up if they don't already exist
if __name__ == '__main__':
    with app.app_context():
        db.create_all() 
    app.run(host='0.0.0.0', port=5000)