FROM python:3.10-slim
MAINTAINER carson.l.fry@gmail.com

WORKDIR /app

# Install all Python packages listed in requirements.txt
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# Using entrypoint instead of cmd in case we want to use cmd to pass parameters in the future
ENTRYPOINT ["python", "app.py"]

